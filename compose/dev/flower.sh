#!/bin/bash

set -o errexit
set -o pipefail

function help() {
  echo "Usage: $(basename "$0") [--port 8888] [--log-level DEBUG] [--prefix flower-root]"
  echo "Runs Flower."
  echo
  echo "Options:"
  echo "   --port          Optional; port to listen to. Default is '5554'."
  echo "   --log-level     Optional; set Celery log level. Default level if INFO."
  echo "   --prefix        Optional; URL prefix. Empty by default."
  echo "                   See: https://flower.readthedocs.io/en/latest/config.html#url-prefix"
  echo
}

PORT=5554
LOG_LEVEL=ERROR
PREFIX=

while [[ $# -gt 0 ]]
do
  case $1 in
    --port)
      PORT="$2"
      shift
      shift
      ;;
    --log-level)
      LOG_LEVEL="$2"
      shift
      shift
      ;;
    --prefix)
      PREFIX="$2"
      shift
      shift
      ;;
    --help)
      help
      exit 0
      ;;
    *)
      echo "ERROR: invalid option: $1"
      echo
      help
      exit 1
      ;;
  esac
done

if test -z "$MESSAGE_BROKER_URL_FILE"
then
    MESSAGE_BROKER_URL=$MESSAGE_BROKER_URL
else
    MESSAGE_BROKER_URL=$(cat "$MESSAGE_BROKER_URL_FILE")
fi

CMD_ARGS="-A backtasks.celery_app --broker=$MESSAGE_BROKER_URL flower -l $LOG_LEVEL --url_prefix=$PREFIX --port=$PORT"

# shellcheck disable=SC2086
celery $CMD_ARGS
