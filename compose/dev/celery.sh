#!/bin/bash

set -o errexit
set -o pipefail

function help() {
  echo "Usage: $(basename "$0") [--queue <QUEUE1,QUEUE2>] [--log-level DEBUG] [--threads 10] [--reload] [--beat]"
  echo "Runs Celery worker."
  echo
  echo "Options:"
  echo "   --queues        Optional; tells Celery to consume tasks from specific queues"
  echo "                   delimited by comma. Default value is 'default,celery'."
  echo "                   'celery' queue is a default queue for periodic tasks handled by beat &"
  echo "                   there must be at least one consumer for that queue."
  echo "   --log-level     Optional; set Celery log level. Default level if INFO."
  echo "   --threads       Optional; the number of threads to run."
  echo "                   By default Celery will run the same number as"
  echo "                   the number of cores available."
  echo "                   See: https://docs.celeryq.dev/en/latest/userguide/workers.html#concurrency"
  echo "   --reload        Flag; optional; auto reload celery process after code was changed."
  echo "                   Should be used in development evnironment only."
  echo "   --beat          Flag; optional; run Celery beat w/ the main Celery process."
  echo "                   Should be used in development evnironment only."
  echo
}

QUEUES=default,celery
LOG_LEVEL=INFO
THREADS=-1
AUTO_RELOAD=0
BEAT=0

while [[ $# -gt 0 ]]
do
  case $1 in
    --queues)
      QUEUES="$2"
      shift
      shift
      ;;
    --log-level)
      LOG_LEVEL="$2"
      shift
      shift
      ;;
    --threads)
      THREADS="$2"
      shift
      shift
      ;;
    --reload)
      AUTO_RELOAD=1
      shift
      ;;
    --beat)
      BEAT=1
      shift
      ;;
    --help)
      help
      exit 0
      ;;
    *)
      echo "ERROR: invalid option: $1"
      echo
      help
      exit 1
      ;;
  esac
done

if test -z "$MESSAGE_BROKER_URL_FILE"
then
    MESSAGE_BROKER_URL=$MESSAGE_BROKER_URL
else
    MESSAGE_BROKER_URL=$(cat "$MESSAGE_BROKER_URL_FILE")
fi

CMD_ARGS="-A backtasks.celery_app --broker=$MESSAGE_BROKER_URL worker -Q $QUEUES -l $LOG_LEVEL -s /tmp/celery-schedule"

if [[ "$BEAT" -eq 1 ]]; then
  CMD_ARGS="$CMD_ARGS -B"
fi

if [[ "$THREADS" -gt -1 ]]; then
  CMD_ARGS="$CMD_ARGS --concurrency=$THREADS"
fi

if [[ "$AUTO_RELOAD" -eq 1 ]]; then
  # shellcheck disable=SC2086
  watchmedo auto-restart --patterns='*.py;*.html' --recursive celery -- $CMD_ARGS
else
  # shellcheck disable=SC2086
  celery $CMD_ARGS
fi
