from typing import Any

from fastapi import status
from pydantic import BaseModel


class Response(BaseModel):
    success: bool = True
    status_code: int = status.HTTP_200_OK


class ResponseData(Response):
    data: Any = {}
    error: dict | None = {}


class ResponseError(Response):
    success: bool = False
    error: dict
