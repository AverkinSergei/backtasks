from typing import Union
from uuid import UUID

from fastapi import APIRouter
from forms.calc import CalcForm
from routers.common import response

from backtasks.repository import get_task_result, get_tasks_stats

tasks_router = APIRouter(prefix="/api/v1/tasks")


@tasks_router.post(
    "/calculate",
    response_model=Union[response.ResponseData, response.ResponseError],
    tags=["Calculate"],
)
async def calculate(form_data: CalcForm) -> response.ResponseData | response.ResponseError:
    return response.ResponseData(data=form_data.run_calculate().as_list())


@tasks_router.get(
    "/{task_id}/status",
    response_model=Union[response.ResponseData, response.ResponseError],
    tags=["Status"],
)
async def task_status(task_id: UUID) -> response.ResponseData | response.ResponseError:
    return response.ResponseData(data=get_task_result(task_id=str(task_id)))


@tasks_router.get(
    "/", response_model=Union[response.ResponseData, response.ResponseError], tags=["Tasks"]
)
async def get_tasks() -> response.ResponseData | response.ResponseError:
    return response.ResponseData(data=get_tasks_stats())
