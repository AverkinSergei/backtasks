from unittest.mock import patch

from backtasks.tasks import calculate


@patch("backtasks.tasks.calculate.run")
def test_mock_task(mock_run):
    assert calculate.run(1)
    calculate.run.assert_called_once_with(1)

    assert calculate.run(2)
    assert calculate.run.call_count == 2
