from pydantic import BaseModel

from backtasks.tasks import calculate


class CalcForm(BaseModel):
    first_operand: float
    second_operand: float
    operator: str

    def run_calculate(self):
        return calculate.apply_async(kwargs=self.model_dump())
