import os

import routers
import sentry_sdk
import uvicorn
from conf.desc import DESCRIPTION, TITLE
from conf.openapi_tags import OPENAPI_TAGS
from fastapi import FastAPI
from fastapi.openapi.utils import get_openapi
from starlette.middleware.cors import CORSMiddleware

sentry_sdk.init(os.environ.get("SENTRY_DSN"))


def create_app():
    app = FastAPI(
        root_url="/api/v1",
        docs_url="/api/v1/docs",
        redoc_url="/api/v1/redoc",
        openapi_url="/api/v1/openapi.json",
        openapi_tags=OPENAPI_TAGS,
    )

    def custom_openapi():
        if app.openapi_schema:
            return app.openapi_schema
        openapi_schema = get_openapi(
            title=TITLE,
            version="0.0.1",
            description=DESCRIPTION,
            routes=app.routes,
        )
        openapi_schema["info"]["x-logo"] = {
            "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
        }
        app.openapi_schema = openapi_schema
        return app.openapi_schema

    app.openapi = custom_openapi

    app.include_router(routers.tasks_router)

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
        expose_headers=["*"],
    )

    return app


app_ = create_app()

if __name__ == "__main__":
    uvicorn.run("main:app_")
