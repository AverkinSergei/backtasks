import os

from celery import Celery

app = Celery(
    "app",
    backend=os.environ.get("REDIS_URL"),
    broker=os.environ.get("MESSAGE_BROKER_URL"),
    include=["backtasks.tasks"],
)

app.autodiscover_tasks(lambda: ["backtasks.tasks"])
