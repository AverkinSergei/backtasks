from celery.result import AsyncResult

from backtasks import celery_app


def get_task_result(task_id: str):
    task_result = AsyncResult(task_id)
    return {
        "task_id": task_id,
        "task_status": task_result.status,
        "task_result": task_result.result,
    }


def get_tasks_stats():
    inspector = celery_app.control.inspect()
    return {
        "registered": inspector.registered(),
        "reserved": inspector.reserved(),
        "active": inspector.active(),
        "scheduled": inspector.scheduled(),
        "revoked": inspector.revoked(),
    }
