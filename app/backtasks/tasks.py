from backtasks import celery_app


@celery_app.task
def calculate(
    first_operand: float | int, second_operand: float | int, operator: str
) -> float | None:
    if operator == "+":
        return first_operand + second_operand
    if operator == "-":
        return first_operand - second_operand
    if operator == "*":
        return first_operand * second_operand
    if operator == "/":
        return first_operand / second_operand
